# 0D_Multicomp #

This MATLAB code evaluates the following characteristics for multicomponent fuels

* Evaporation - Droplet mass, composition and temperature with time
* Distillation - Flash and fractional calculations 

Please refer to the [IJHMT paper](http://www.sciencedirect.com/science/article/pii/S0017931016309887) for implementation details.
## How do I get set up? ##

If this repository is downloaded using `clone --recursive`, the [GroupContribution](https://github.com/gpavanb/GroupContribution) library is automatically downloaded.  

First ensure that MATLAB-CANTERA is correctly installed and is specified in `main.m`. Then, once you are in 'src' folder, you can simply type

```
>> main('posf10325','posf10325_init')
```
where the first argument stands for the compound description (full list in data/Compound_Descriptions of GroupContribution) and the second for the initial composition (full list in data/Init_Data of GroupContribution) for evaluating droplet vaporization

A similar procedure can be used for flash and fractional distillation

```
>> distMoleFrac('posf10325','posf10325_init')
```

## License ##

Please refer to the LICENSE.pdf in the repository. Note that this code requires PRIOR PERMISSION FROM AUTHORS FOR COMMERCIAL PURPOSES.

## Who do I talk to? ##

* Repo owner or admin : [Pavan Bharadwaj](https://bitbucket.org/gpavanb)
* Other community or team contact : The code was developed at the Flow Physics and Computational Engineering group at Stanford University. Please direct any official queries to [Prof. Matthias Ihme](mailto:mihme@stanford.edu)

## References ##
[1] Govindaraju, Pavan B., and Matthias Ihme. "Group contribution method for multicomponent evaporation with application to transportation fuels." International Journal of Heat and Mass Transfer 102 (2016): 833-845.
