function [ ret ] = activity(xVec,T)

global propMat a num_comp numGroups funcMat;

% R TABLE
R = propMat(11,:);
assert(length(R) == numGroups);

% Q TABLE
Q = propMat(12,:);
assert(length(Q) == numGroups);

% ENERGY INTERACTION PARAMETER
Psi = exp(-a/T);

% COORDINATION NUMBER
z = 10;

% r VECTOR
r = (funcMat*R')';

% q VECTOR
q = (funcMat*Q')';

% L VECTOR
LVec = (z/2)*(r - q) - (r - 1);

% THETA VECTOR
thetaVec = xVec.*q/dot(xVec,q);

% PHI VECTOR
phiVec = xVec.*r/dot(xVec,r);

% COMBINATORIAL ACTIVITY
gammaCVec = exp(log(phiVec./xVec) + (z/2)*q.*log(thetaVec./phiVec) + LVec ... 
            - (phiVec./xVec)*dot(xVec,LVec));
        
% SET ACTIVITY OF COMPLETELY VAPORIZED SPECIES TO 1        
gammaCVec(isnan(gammaCVec)) = 1;
        
%------------------------------------------
% RESIDUAL ACTIVITY ROUTINE
%------------------------------------------
                     
% CALCULATE GROUP MOLE FRACTION
XVec = xVec*funcMat;
XVec = XVec/sum(XVec);
        
% CALCULATE Theta VECTOR
ThetaVec = Q.*XVec/dot(Q,XVec);
        
% CALCULATE FUNCTIONAL GROUP AND ISOLATED GAMMAS

% AVOID DIVISION-BY-ZERO IN LAST TERM
divRes = ThetaVec./(ThetaVec*Psi);
divRes(isnan(divRes)) = 0;

GammaVec = Q.*(1 - log(ThetaVec*Psi) - divRes*Psi' );
GammaIVec = zeros(num_comp,numGroups);

% REFER TO IJHMT PAPER FOR DETAILS
for i = 1:num_comp
    XIVec = funcMat(i,:)/sum(funcMat(i,:));
    ThetaIVec = Q.*XIVec/dot(Q,XIVec);
    
    divRes = ThetaIVec./(ThetaIVec*Psi);
    divRes(isnan(divRes)) = 0;
    
    GammaIVec(i,:) = Q.*(1 - log(ThetaIVec*Psi) - divRes*Psi' );

end

% RESIDUAL ACTIVITY
gammaRVec = zeros(1,num_comp);
for i = 1:num_comp
    gammaRVec(i) = exp( dot( funcMat(i,:) , GammaVec - GammaIVec(i,:)  ) );
end

% TOTAL ACTIVITY
ret = gammaCVec.*gammaRVec;

end

