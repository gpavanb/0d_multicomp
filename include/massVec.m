function [ret] = massVec(fuel,r,xVec,T)
% This function returns the number of moles of each
% component in the liquid phase of a droplet given the 
% radius and mole fractions

% SPECIFIC VOLUME GOES WITH MOLE FRACTIONS
% NUMBER OF MOLES*MOLE FRACTION*MW
vol = (4/3)*pi*(r^3);
if (vol > 0)
    ret = ( vol/dot(fuel.specVol(T),xVec) )*xVec.*fuel.MW;
else
    ret = zeros(length(xVec));
end

