function [k] = StielThodos(invK,k0,rho_r,Z)

% PRESSURE CORRECTION TO GAS THERMAL CONDUCTIVITY
% USING STIEL-THODOS METHOD

if (rho_r < 0.5)
    k = (1.22e-2 * (exp(0.535*rho_r) - 1) / (invK * Z^5) ) + k0;
elseif (rho_r < 2)
    k = (1.14e-2 * (exp(0.67*rho_r) - 1.069) / (invK * Z^5) ) + k0;
elseif (rho_r < 2.8)
    k = (2.60e-3 * (exp(1.155*rho_r) + 2.016) / (invK * Z^5) ) + k0;
else
    disp('Exceeded Range of Stiel-Thodos Pressure Correction');
    return;
end

