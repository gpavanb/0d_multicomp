function [rec,moles] = RecFrac(x_l,p,T,MW,pSatVec,gammaVec)

% PERFORM FLASH CALCULATION TO OBTAIN
% RECOVERED MASS FRACTION

% % MAXWELL-BONNELL
% C = (TbVec/341.9 - 1.0).*(10.15 - 0.1285*TbVec);
% B = ((1.0./(3870+C))-341.9./(3870.*(TbVec+C)))./(1.0 - (341.9/3870));
% TE = 341.9*(1.0./(TbVec + C) - B)./(1.0./(T + C) - B);
% ALOGPN = (0.1047 + 259.8./(TE + 55)).*TE-6.074*(TE.^0.5)-140.65;
% SGN = 1.0475-1.511e-4*TbVec+7.127e-8*TbVec.*TbVec-116.4./TbVec;
% TRAT = 341.9./TbVec;
% ALOGPN = ALOGPN + (sqrt(SgVec./SGN)-1).*((TbVec/T).^3 - 1).*(3.214 ...
%          -3.765*(TRAT.^2));
% pSatVec = exp(ALOGPN)*101325/760;

% COMPUTE VAPOR MOLE FRACTION
KVec = gammaVec.*pSatVec/p;
bufVec = 1./(KVec-1);

syms lhs bet;
lhs = 0;

% SOLVE NON-LINEAR EQUATION
% REFER TO IJHMT PAPER FOR DETAILS
for i = 1:length(x_l)
    lhs = lhs + ( x_l(i)./(bufVec(i) + bet) );
end

rec = double(vpasolve(lhs==0, bet,[0 1]));
if (isempty(rec))
    rec = 0;
end

xVec = x_l./(1 + rec*(KVec-1));
yVec = KVec.*x_l;

% CONVERT TO MASS FRACTION AND CALCULATE RECOVERED MASS FRACTION
moles = yVec/sum(yVec);
rec = dot(MW,rec*yVec)/dot(MW,(1-rec)*xVec + rec*yVec);

end

