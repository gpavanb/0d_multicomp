function [Vm] = PR(Pc,Tc,omega,P,T)

R_g = 8.314;

% BACKS OUT SPECIFIC MOLAR VOLUME BASED ON
% PENG-ROBINSON EQUATION

len = length(Pc);
Vm = zeros(1,len);


for i = 1:len
Tr = T/Tc(i);
m = 0.3764 + 1.54226*omega(i) - 0.26992*omega(i)*omega(i);

alfa = (1 + m*((1 - sqrt(Tr))) )^2;
a = 0.457235 * alfa * (R_g^2) * (Tc(i)^2)/Pc(i);
b = 0.077796*R_g*Tc(i)/Pc(i);
A = a*P/(R_g^2 * T^2);
B = b*P/(R_g * T);

% NUMERICAL ERROR GIVES SLIGHTLY IMAGINARY FOR REAL ROOTS
r = solveCubic(B^3 + B^2 - A*B,A-2*B-3*B*B,B-1);

% TAKE MAXIMUM OR MINIMUM ROOT DEPENDING ON PHASE
% CURRENTLY USING FOR LIQUID
Vm(i) = min(r)*R_g*T/P;
end

end

