function [ ret ] = radius(fuel,massVec,T)

rho = fuel.MW./fuel.specVol(T);

vol = sum( (massVec./rho) );
if (vol > 0)
    ret = (3*vol/(4*pi))^(1/3);
else
    ret = 0;
end

