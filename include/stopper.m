function [value,isterminal,direction] = stopper(t,inputVec,fuel)

global num_comp;

% SEPARATE MASSES AND TEMPERATURE
massVec = inputVec(1:num_comp);
m = sum(massVec);
T = max(0,inputVec(num_comp+1));

% PROGRAM WRITTEN FOR ROW VECTORS
massVec = massVec';   

% COMPUTE RADIUS
r = radius(fuel,massVec,T);

value = [m r];
isterminal = [1 1];
direction = [0 0];

end