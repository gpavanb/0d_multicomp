function [ ret ] = moleFracVec(fuel,massVec)

% TODO : Documentation

ret = (massVec./fuel.MW);

if (sum(ret) ~= 0)
    ret = ret/sum(ret);
end
end

