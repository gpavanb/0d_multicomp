function [k] = RoyThodos(invK,Tr)

% THERMAL CONDUCTIVITY AT STANDARD PRESSURE USING
% ROY-THODOS METHOD

k = 8.757*(exp(0.0464*Tr) - exp(-0.2412*Tr))/invK;

end

