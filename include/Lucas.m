function [mu] = Lucas(Tr,xi)

% VISCOSITY AT STANDARD PRESSURE USING LUCAS 1984a

mu = (((0.807*(Tr.^0.618) - 0.357*exp(-0.449*Tr) + ...
       0.340*exp(-4.058*Tr) + 0.018))./xi);
end

