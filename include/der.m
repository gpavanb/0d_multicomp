function [ret] = der(t,inputVec,fuel)

% SEPARATE MASSES AND TEMPERATURE
massVec = inputVec(1:end-1);
m = sum(massVec);
T = max(0,inputVec(end));

% PROGRAM WRITTEN FOR ROW VECTORS
massVec = massVec';

% LIST OF GLOBAL VARIABLES
global num_comp...
       p T_amb ...
       niFlag evapModelFlag propertyMode...
       MW_air rho_g mu_g k_g cp_g gas ;
   
x_l = moleFracVec(fuel,massVec);

MW = fuel.MW;
c_lVec = fuel.c_l(T);
if (propertyMode == 0)
    LVec = fuel.L;
else
    LVec = fuel.L(T);
end
pSatVec = fuel.Psat(T);

% CALCULATE PHYSICAL PROPERTIES OF DROPLET
% TODO : Currently using liquid mass fractions for calculating
% effective mixture properties (vapor makes it implicit)
% CALCULATE PHYSICAL PROPERTIES OF DROPLET
y_l = MW.*x_l/dot(MW,x_l);
cl = dot(y_l,c_lVec);

% COMPUTE RADIUS
r = radius(fuel,massVec,T);

% VELOCITY
U = 0;

% COMPUTE ACTIVITIES
if (niFlag == 1)
   gammaVec = fuel.activity(x_l,T);
else
   gammaVec = ones(1,num_comp);
end

% COMPUTE DERIVATIVES

%% MASS TRANSFER ANALOGY
if (evapModelFlag == 0)
    % GET DIFFUSION
    DVec = fuel.D(p,(2/3)*T + (1/3)*T_amb);
    
    % COMPUTE PARTIAL PRESSURE
    pVec = gammaVec.*x_l.*pSatVec;
    
    % NON-DIMENSIONAL NUMBERS
    Pr = cp_g*mu_g/k_g;
    Sc = 0.7;
    Re = 2*rho_g*r*U/mu_g;
    
    % COMPUTE SHERWOOD NUMBER
    Sh = 2 + 0.6*(Re^0.5)*(Sc^(1/3));
    kcVec = Sh*DVec/(2*r);
    
    % COMPUTE CsVec
    CsVec = pVec/(8.314*T);
    
    % COMPUTE CinfVec
    CinfVec = zeros(1,num_comp);
    
    % EVAPORATION RATE
    dmdt = -(4*pi*r*r)*MW.*kcVec.*(CsVec - CinfVec);
    
    % COMPUTE TEMPERATURE DERIVATIVE
    % Nu definition taken from Bader et al. 2013
    Nu = 2 + 0.6*(Re^0.5)*(Pr^(1/3));
    if (r == 0)
        der_T = 0;
    else
        der_T = (dot(dmdt,LVec) + (2*pi*Nu*k_g*r)*(T_amb - T))/(cl*sum(massVec));
    end

%% CUMULATIVE SPALDING NUMBER    
else
    pv = gammaVec.*x_l.*pSatVec;
    
    % x_v CONTAINS LOCAL VAPOR COMPOSITION
    % OF SPECIES ALSO IN DROPLET
    % TODO : SET THIS
    xv = zeros(size(x_l));
    
    % COMPUTE RELEVANT VAPORIZATION PARAMETERS
    % SET GAS
    set(gas,'T',(2/3)*T + (1/3)*T_amb);
    
    % COMPUTE GAS PROPERTIES
    rho_g = density(gas); % (Kg/m3)
    cp_g = cp_mass(gas);
    k_g = thermalConductivity(gas);
    
    % MOMENTUM RELAXATION TIME
    rho_d = 0.75*sum(massVec)/(pi*(r^3));
    tau_d = 4*rho_d*(r^2)/(18*mu_g);
    
    % COMPUTE NON-DIMENSIONAL NUMBERS
    Pr = cp_g*mu_g/k_g;
    Sc = 0.7;
    Re = 2*rho_g*r*U/mu_g;
    
    % NUSSELT AND SHERWOOD NUMBERS
    Nu = 2 + 0.6*(Re^0.5)*(Pr^(1/3));
    Sh = 2 + 0.6*(Re^0.5)*(Sc^(1/3));
    
    % NOTE THAT dmdt*dt*eps_i CAN BE LARGER THAN WHAT IS IN THE DROPLET
    % THE NON-NEGATIVE CONSTRAINT IS THUS NECESSARY
    
    % BOILING REGIME
    if (sum(pv) >= p)
        der_T = 0;
        
        % pv EXCEEDS BY TIMESTEP ERROR JUST BEFORE BOILING OCCURS
        % NEED TO NORMALIZE
        pv = p*(1-sum(xv))*pv/sum(pv);
        Xsat = pv/p;
        eps_i = Xsat.*MW/dot(Xsat,MW);
        disp(strcat('Boiling! Mass rate split: ',num2str(eps_i)));
         
        % CORRECTION FACTOR SKIPPED BECAUSE IT BECOMES A TRANSCENDENTAL
        % EQUATION
        % CALCULATE MASS FLOW RATE BASED ON CONVECTIVE HEAT TRANSFER
        mdot = sum(massVec)*cl*(Nu/(3*Pr))*(cp_g/cl)*(1/tau_d)* ...
               (T-T_amb)/dot(LVec,eps_i);
        dmdt = mdot*eps_i;
        
    % EVAPORATION REGIME    
    else

        % CONVERT TO MASS FRACTIONS
        Xsat = pv/p;
        X_air = 1-sum(Xsat)-sum(xv);
        mass = dot(Xsat+xv,MW) + X_air*1e-3*MW_air;
        Ysat = Xsat.*MW/mass;
        yv = xv.*MW/mass;
        
        % COMPUTE MASS SPALDING NUMBER
        Bm = (sum(Ysat) - sum(yv))/(1-sum(Ysat));
    
        % COMPUTE EVAPORATION RATE
        dmdt = -(Sh/(3*Sc))*(sum(massVec)/tau_d)*log(1 + Bm)*Ysat/sum(Ysat);
        
        % COMPUTE CORRECTION FACTOR
        beta = 0;
        if (sum(massVec) <= 0 || r <= 0)
            beta = 0;
        else
            beta = -1.5*Pr*sum(dmdt)/(sum(massVec)/tau_d);
        end
        
        f2 = 0;
        if (beta ~= 0)
            f2 = beta/(exp(beta)-1);
        end
        
        % COMPUTE TEMPERATURE DERIVATIVE
        der_T = (Nu/(3*Pr))*(cp_g/cl)*(f2/tau_d)*(T_amb - T) + ...
            dot(dmdt,LVec)/(sum(massVec)*cl);
        
    end

end
    
ret = [dmdt' ; der_T];

end
