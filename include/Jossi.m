function [mu] = Jossi(mu0,rho_r,xi_T)

% PRESSURE CORRECTION FOR VISCOSITY BASED ON
% JOSSI 1962

mu = (((1.0230 + 0.23364 * rho_r + 0.58533 * rho_r.^2 - ...
        0.40758 * rho_r.^3 + 0.0933324 * rho_r.^4).^4 - 1)./xi_T) + mu0;

end

