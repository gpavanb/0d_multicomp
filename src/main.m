function main(funcName,initName)
% funcName corresponds to palette
% initName corresponds to initList (initial mole fractions in order)

disp(initName);

% ACTIVATE MATLAB-CANTERA
orig_dir = pwd;
chdir('/Users/gpavanb/Cantera/lib/cantera/matlab/toolbox/');
ctpath;
chdir(orig_dir);

% ADD PATHS
addpath('../include');
addpath('../include/GroupContribution');
addpath('../include/CorrelationProp');

% LIST OF GLOBAL VARIABLES
global p num_comp numGroups r_0 x_l_init ...
       T_amb a propMat funcMat niFlag ...
       Pc_air Vc_air Tc_air Zc_air MW_air Sigma_air evapModelFlag ...
       rho_g k_g mu_g cp_g gas propertyMode;

% CREATE GAS
gas = GRI30('Mix');
   
% TIME OF SIMULATION
tEnd = 0.075;

% PROPERTIES SOURCE
% 0 for GROUP CONTRIBUTION
% 1 for OPENSMOKE CORRELATION
propertyMode = 0;

% PALETTE AND INITIAL LIST MUST BE INPUT FOR CORRELATION MODE
% palette = funcName;
% initList = initName;
palette = {'IC8H18','C7H8','C7H16'};
initList = [0.56,0.28,0.16];

%% READ EXCEL FILES

if (propertyMode == 0)
    fuel = GroupContribution(funcName,initName);
    % READ UNIFAC PARAMETERS
    propMat = fuel.propMat;
    numGroups = size(propMat,2);
    
    % FUNCTIONAL GROUP TABLE
    funcMat = fuel.funcMat;
else
    fuel = CorrelationProp(palette,initList);
end

num_comp = fuel.num_comp;

% COMPONENT MOLECULAR WEIGHTS
MW = fuel.MW;
    
% INITIAL LIQUID MASS FRACTIONS
x_l_init = fuel.x_l_init;

%% SYSTEM CONDITIONS

% IDEAL GAS CONSTANT
R_g = 8.314; % (J/mol/K)

% SYSTEM PRESSURE (Pa)
p = 1e5;  

% SYSTEM TEMPERATURE (K)
T_amb = 833;

% INITIAL DROPLET RADIUS (m)
r_0 = 18.5e-6;

% INITIAL DROP TEMPERATURE (K)
T_init = 298; 

% SET NON-IDEAL FLAG (1 for UNIFAC)
niFlag = 0;

% SET EVAPORATION MODEL FLAG
% 1 for CUMULATIVE SPALDING NUMBER
% 0 for MASS TRANSFER ANALOGY
evapModelFlag = 1;

% SET GAS
set(gas,'T',T_amb,'P',p,'X','O2:1,N2:3.76');

%% AIR PROPERTIES

% STANDARD PROPERTIES
MW_air = meanMolecularWeight(gas); % (g/mol)
Sigma_air = 3.711e-10; %(A)

% AIR CRITICAL PROPERTIES
Pc_air = 37.71; % (bar)
Vc_air = 92.35e-6; % (m3/mol)
Tc_air = 132.65;
Zc_air = Pc_air*1e5*Vc_air/(R_g*Tc_air);

%% AIR PROPERTY CALCULATIONS

rho_g = density(gas); % (Kg/m3)
cp_g = cp_mass(gas);
k_g = thermalConductivity(gas);

Tr_air = T_amb/Tc_air;
rho_r = Vc_air*molarDensity(gas);

% AIR VISCOSITY
xi_T = (Tc_air/((MW_air^3)*(Pc_air^4)))^(1/6);
xi = 0.176*xi_T;
mu_g = Lucas(Tr_air,xi);
% PRESSURE CORRECTION
mu_g = Jossi(mu_g,rho_r,xi_T)*1e-7;

%% MULTICOMPONENT CRITICAL PROPERTIES
a = fuel.a;

%% ODE SOLVE

% COMPUTE INITIAL MASS VECTOR
massVec_init = massVec(fuel,r_0,x_l_init,T_init);

% CREATE INITIAL INPUT VECTOR
inputVec_init = [massVec_init T_init];

% TIMESTEPPING - RK4
options = odeset('NonNegative',1:num_comp+1,'RelTol',2e-5,'Events',@(t,y)stopper(t,y,fuel));
[T, massVecMat] = ode23(@(t,y)der(t,y,fuel),[0 tEnd],inputVec_init,options);

massVecMat = massVecMat(:,1:num_comp+1);
%% POST PROCESSING

% CALCULATE RADII AT EACH TIME STEP (NOTE TEMPERATURE VARIABLE)
radMat = zeros(size(massVecMat,1)-1,1);
moleMat = zeros(size(massVecMat,1)-1,num_comp);

for i = 1:size(massVecMat,1)
    radMat(i) = radius(fuel,massVecMat(i,1:end-1),massVecMat(i,end));
end

for i = 1:size(massVecMat,1)
    moleMat(i,:) = moleFracVec(fuel,massVecMat(i,1:end-1));
end

figure;
plot(T,radMat.^2/r_0^2);
figure;
plot(T,massVecMat(:,num_comp+1));

dumpMat = [T,2*radMat,massVecMat(:,4),massVecMat(:,1:3)];

end
