% PROGRAM TO CALCULATE DISTILLATION CURVES (FLASH, FRACTIONAL AND STEP-VOLUME)

% PROTOTYPES ARE
% distMoleFrac(funcName, save_output, initName, dist_type)
% distMoleFrac(funcName, save_output, init_comp, expData, dist_type)

function [varargout] = distMoleFrac(funcName,save_output,varargin)

% SETUP FOR INITNAME
if length(varargin)==2
    argFlag = 0;
    initName = varargin{1};
    dist_type = varargin{2};
% SETUP FOR OBJECTIVE FUNCTION
elseif length(varargin) == 3
    init_comp = varargin{1};
    init_comp = init_comp/sum(init_comp);
    expData = varargin{2};
    % LOAD EXPERIMENTAL DATA
    dumpMatS = load(strcat('../Output/',expData,'.csv'));
    dist_type = varargin{3};
    argFlag = 1;
else
    disp('Invalid arguments specified for distMoleFrac');
    return;
end

% ADD PATHS
addpath('../include','../Plotters');
addpath('../include/GroupContribution');

%% READ EXCEL FILES

% READ UNIFAC PARAMETERS
if (argFlag == 0)
    fuel = GroupContribution(funcName,initName);
elseif (argFlag == 1)
    fuel = GroupContribution(funcName);
end

% INITIAL LIQUID MASS FRACTIONS
if (argFlag == 0)
    x_l_init = fuel.x_l_init;
elseif (argFlag == 1)
    x_l_init = init_comp;
end

%% SYSTEM CONDITIONS
% SYSTEM PRESSURE (Pa)
p = 0.1e6;

% FINAL TEMPERATURE (K)
T_fin = 600;

% INITIAL TEMPERATURE (K)
T_init = 270;

% NON-IDEAL FLAG (1 FOR UNIFAC)
niFlag = 0;

% TEMPERATURE VECTOR
dT = 1;
tempVec = T_init:dT:T_fin;
%% MULTICOMPONENT CRITICAL PROPERTIES

% CREATE BOILING POINT VECTOR
% LEE-KESLER SATURATION VAPOR PRESSURE
MW = fuel.MW;
TbVec = fuel.Tb(p);

% % RECOVERED FLASH FRACTION
if (strcmp(dist_type,'flash'))
    recF = zeros(length(tempVec),1);
    moleMatF = zeros(length(tempVec),length(x_l_init));
    
    for i = 1:length(tempVec)
        T = tempVec(i);
        pSatVec = fuel.Psat(T);
        % COMPUTE ACTIVITIES
        if (niFlag == 1)
            gammaVec = fuel.activity(x_l_init,T);
        else
            gammaVec = ones(1,fuel.num_comp);
        end
        [recF(i),moleMatF(i,:)] = RecFrac(x_l_init,p,T,MW,pSatVec,gammaVec);
    end
    % FLASH OUTPUT
    % TRUNCATE
    resMat = [tempVec',recF];
    [C,ia,ic] = unique(resMat(:,2));
    resMat = resMat(ia,:);
    figure;
    hold on;
    plot(resMat(:,2),resMat(:,1));
    if (save_output)
        save(strcat('../Output/',initName,'_i_dist.mat'),'resMat');
    end
end
    
if (strcmp(dist_type,'fractional'))
    % RECOVERED FRACTIONAL FRACTION
    tVec = T_init:dT:T_fin;
    recFr = zeros(length(tempVec),1);
    moleMatFr = zeros(length(tVec),length(x_l_init));
    massTot = dot(fuel.MW,x_l_init);
    x_l = x_l_init;
    for i = 1:length(tVec)
        % REMOVE BOILING SPECIES
        x_l( (tVec(i)./TbVec >= 1) & (x_l ~=0) ) = 0;
        
        massFrac = dot(x_l,fuel.MW)/massTot;
        recFr(i) = 1-massFrac;
        moleMatFr(i,:) = x_l/sum(x_l);
    end
    % FRACTIONAL OUTPUT
    % TRUNCATE
    resMat = [tVec',recFr];
    plot(resMat(:,2),resMat(:,1));
    if (save_output)
        save(strcat('../Output/',initName,'_fracDist.mat'),'resMat');
    end 
end

if (strcmp(dist_type,'step_volume'))
    % STEP-VOLUME DISTILLATION
    vStep = 0.01;
    recTS = [];
    recFS = [];
    moleMatFS = [];
    moleVec = x_l_init;
    massTot = dot(fuel.MW,x_l_init);
    x_l = x_l_init;
    syms T;
    while(any(moleVec)~=0)
        Tsol = double(vpasolve(dot(x_l,fuel.Psat(T)) == p,T,[T_init,T_fin]));
        if (isempty(Tsol))
            disp('Solution out of specified range. Change and try again');
        end
        recTS = [recTS;Tsol];
        
        % GET VAPOR COMPOSITION
        xv = fuel.Psat(Tsol).*x_l/p;
        
        % UPDATE LIQUID COMPOSITION
        moleVec = max(0,moleVec - vStep*xv);
        x_l = moleVec/sum(moleVec);
        moleMatFS = [moleMatFS;x_l];
        
        % CALCULATE RECOVERED MASS FRACTION
        massFrac = dot(moleVec,fuel.MW)/massTot;
        recFS = [recFS;1-massFrac];
    end
    % STEP-VOLUME OUTPUT
    resMat = [recTS,recFS];
    [C,ia,ic] = unique(resMat(:,2));
    resMat = resMat(ia,:);
    plot(resMat(:,2),resMat(:,1));
    if (save_output)
        save(strcat('../Output/',initName,'_stepDist.mat'),'resMat');
    end
end

if (argFlag == 1)
    % CALCULATE ERROR NORM
    num_pts = 50;
    exp_lim = [min(dumpMatS(:,1)),max(dumpMatS(:,1))];
    xq = linspace(exp_lim(1),exp_lim(2),num_pts);
    
    % TRUNCATE BEFORE INTERPOLATION
    [C,ia,ic] = unique(resMat(:,2));
    resMat = resMat(ia,:);
    
    res = interp1(resMat(:,2),resMat(:,1),xq,'previous');
    exp = interp1(dumpMatS(:,1),dumpMatS(:,2),xq,'previous');
    diff = trapz(xq,abs(exp-res));
    varargout{1} = diff;
end

end
